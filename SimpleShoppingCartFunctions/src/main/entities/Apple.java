package main.entities;

public class Apple extends Fruit{

	private static float price = (float) 0.60; 
	
	public Apple() {
		
	}

	public static float getPrice() {
		return price;
	}

	public static void setPrice(float price) {
		Apple.price = price;
	}
	
}

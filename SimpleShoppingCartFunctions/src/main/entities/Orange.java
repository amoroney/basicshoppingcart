package main.entities;

public class Orange extends Fruit {
	
	private static float price = (float) 0.25; 

	public Orange() {
		
	}

	public static float getPrice() {
		return price;
	}

	public static void setPrice(float price) {
		Orange.price = price;
	}
	
	
	
}

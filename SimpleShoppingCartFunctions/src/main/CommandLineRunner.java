package main;

import java.util.ArrayList;
import java.util.List;

import main.entities.*;
import main.functions.*;

public class CommandLineRunner {

	public static void main(String[] args) {
		
		List<Fruit> input = new ArrayList<Fruit>();
		Orange o1 = new Orange();
		Orange o2 = new Orange();
		Apple a1 = new Apple();

		Orange o3 = new Orange();

		input.add(o1);
		input.add(o2);
		input.add(a1);
		input.add(o3);
		
		String output = CartFunctions.calculatePrice(input);
				
		System.out.println(output);

	}

}

package main.functions;

import java.util.List;

import main.entities.*;

public class CartFunctions {
	
	public static String calculatePrice(List<Fruit> fruits) {
				
		int apples=0;
		int oranges=0;
		float appTotal=0;
		float orrTotal=0;
		for(Fruit f : fruits) {
			if (f.getClass()==Apple.class) {
				apples++;
			}if(f.getClass()==Orange.class) {
				oranges++;
			}
		}
		
		appTotal = apples*Apple.getPrice();
		orrTotal = oranges*Orange.getPrice();
		appTotal= bogof(apples, appTotal);
		orrTotal = threeForTwo(oranges, orrTotal);		
		
		String result = "�"+(orrTotal+appTotal);
		
		return result;
	}
	
	public static float bogof(int apples, float appTotal) {
		
		appTotal = appTotal/2;
				
		if(apples%2==1) {
			System.out.println("Suggest getting a free apple");
			float bogofprice = ((apples-1)*Apple.getPrice())/2;
			appTotal = Apple.getPrice()+bogofprice;
		}
		return appTotal;
	}
	
	public static float threeForTwo(int oranges, float orrTotal) {
		
		if(oranges%3==0) {
			
			oranges=(oranges/3)*2;
			orrTotal = oranges*Orange.getPrice();
			
		}else {
			if(oranges%3==2) {
				oranges++;
				oranges=(oranges/3)*2;
				orrTotal = oranges*Orange.getPrice();
				System.out.println("Suggest getting a free orange");
			}else{
				oranges=oranges-1;
				oranges=(oranges/3)*2;
				orrTotal = oranges*Orange.getPrice()+Orange.getPrice();
			}
		}
		return orrTotal;
	}
	
}
